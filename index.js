const parseXls = require('./controllers/xlsToJson');
const parseProduct = require('./controllers/parseProduct');
const fs = require('fs');
const createXls = require('./controllers/exportToXls');

let testFile = './src/test.xlsx';
let count = 0;
let newJSON = [];
    parseXls(testFile)
    .then(function (data) {
        return new Promise(function (resolve, reject) {
            (async () => {
                for (const product of data) {
                    count++;
                    await parseProduct(product).then(function (newProd) {
                        newJSON.push(newProd);
                        console.log(count + ' of ' + data.length);
                    }).catch(function () {
                        fs.writeFile('./output/newValues_err.json', JSON.stringify(newJSON), function (err) {
                            console.log('Новый JSON_err создан из-за ошибки на ' + count + ' of ' + data.length);
                            if (err) {
                                console.log('ошибка при записи newValues_err');
                                throw err;
                            }
                        });
                    });
                }
                resolve(newJSON);
            })();
        });
    })
    .then(function (newData) {
        fs.writeFile('./output/newValues_success.json', JSON.stringify(newData), function (err) {
            if (err) {
                console.log('Ошибка при записи newValues_success');
                throw err;
            }
            console.log('Новый JSON_success создан в ' + new Date());
            createXls(newData);
        });
    });


