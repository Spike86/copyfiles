const readXlsxFile = require('read-excel-file/node');

function parseXls(file) {
    return new Promise(function (resolve, reject) {
        readXlsxFile(file)
            .then((rows) => {
            const newArr = [];
            let rowsLength = rows.length;
            for (let i = 1; i < rowsLength; i++) {                
                let item = {
                    name: rows[i][0],
                    imgs: []
                };
                for (let m = 1; m < rows[i].length; m++) {
                    if (rows[i][m] !== null) {
                        item.imgs.push(rows[i][m]);
                    }
                }
                newArr.push(item);
            }
            resolve(newArr);
        })
            .catch((e) => reject(console.log('Не удалось парсить Excel. Ошибка:  ' + e)));
    })
}

module.exports = parseXls;