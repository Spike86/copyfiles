const uploadImage = require('./uploadImage');
const resizeImage = require('./resizeImage');

function loop(product) {
    return new Promise(function (resolve, reject) {
        product['new_urls'] = [];
        (async () => {
            for (const img of product['imgs']) {
                await resizeImage(img)
                    .then(function (stream) {
                        return new Promise(function (resolve, reject) {
                          resolve(uploadImage(stream, product).catch(function (err) {
                              console.log(err)
                          }));
                        });
                    })
                    .then(function (newUrl) {
                        product['new_urls'].push(newUrl)
                    })
                    .catch(function (err) {
                        console.log(err)
                    });
                }
            resolve(product);
        })();
    })
}

module.exports = loop;