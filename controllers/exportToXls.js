const xl = require('excel4node');
const wb = new xl.Workbook();
const ws = wb.addWorksheet('Sheet 1');

ws.cell(1, 1)
  .string('SKU');

ws.cell(1, 2)
  .string('img1');

ws.cell(1, 3)
  .string('img2');

ws.cell(1, 4)
  .string('img3');

ws.cell(1, 5)
  .string('img4');

ws.cell(1, 6)
  .string('img5');

ws.cell(1, 7)
  .string('img6');

function writeXls(images) {
    for (let i = 0; i < images.length; i++) {
        let string = i + 2;
        let SKU = images[i].name;
        ws.cell(string, 1)
            .string(SKU);
        for ( let j = 0; j < images[i]['new_urls'].length; j++) {
            let cell = j + 2;
            let newUrl = images[i]['new_urls'][j];
            ws.cell(string, cell)
                .string(newUrl);
        }
    }

    wb.write('./output/new_values.xlsx');
}

module.exports = writeXls;