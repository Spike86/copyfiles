const request = require('request');
const sharp = require('sharp');

function resize(img) {
    return new Promise(function (resolve, reject) {
        const metaReader = sharp();
        metaReader
            .metadata()
            .then(function (data) {
                return new Promise(function (resolve, reject) {
                    resolve(data);
                })
            })
            .then(function (data) {
                resolve(metaReader.resize(data.width * 2, data.height * 2));
            });

        request
            .get(img)
            .on('error', err => {
                reject(err)
            })
            .pipe(metaReader)
    });
}

module.exports = resize;