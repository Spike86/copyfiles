const {Storage } = require('@google-cloud/storage');
const config = require('../config/config');


const storage = new Storage ({
    projectId: config.cloudProjectID,
    keyFilename: config.cloudKyefile,
});

const bucketName = 'anostor';

function uploadImage(trgt, product) {
    return new Promise(function (resolve, reject) {
        let options = {
            metadata: {
                contentType: 'image/jpeg'
            },
            public: true
        };
        const fileName = product['name'] + '_' + Date.now() + '.jpeg';
        const bucket = storage.bucket(bucketName);
        const file = bucket.file(fileName);

        trgt.pipe(file.createWriteStream(options));
        trgt.end();
        resolve(getPublicUrl(bucketName, fileName));
    });
}

function getPublicUrl (bucketName, filename) {
    return `https://storage.googleapis.com/${bucketName}/${filename}`;
}

module.exports = uploadImage;